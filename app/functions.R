load_data <- function(path2data){
  data <- list()
  # SNDS nodes table
  data$snds_nodes <- read.csv(paste0(path2data, "snds_nodes.csv"),
                        encoding = "UTF-8")
  # SNDS links table
  data$snds_links <- read.csv(paste0(path2data, "snds_links.csv"),
                        encoding = "UTF-8")
  # SNDS variables table
  data$snds_vars <- read.csv(paste0(path2data,"snds_vars.csv"), 
                       encoding = "UTF-8", 
                       stringsAsFactors = FALSE)
  data$snds_vars$table <- as.factor(data$snds_vars$table)
  
  # SNDS tables
  data$snds_tables <- read.csv(paste0(path2data, "snds_tables.csv"),
                          encoding = "UTF-8")
  data$snds_tables$Libelle <- as.character(data$snds_tables$Libelle)
  
  data$snds_nomenclatures <- read.csv(paste0(path2data, "snds_nomenclatures.csv"),
                                      encoding = "UTF-8")
  
  return(data)
}

# Utils functions for feeding DT functions
## Preprocess snds tables
get_snds_tables <- function(snds_tables){
  df <- snds_tables %>% 
    select(one_of('Produit', 'Table', 'Libelle'))
  return(df)
}
## Preprocess snds vars
get_snds_vars <- function(snds_vars){
  df <- snds_vars %>% 
    select(one_of("table", "var", "description", "nomenclature"))
  return(df)
}

# Functions for ElasticSearch queries
get_query_result_agg_by_index <- function(term, snds_nomenclatures, elastic_connexion){
  
  if (term==''){
    term = '*'
  }
  
  aggs <- list(
    aggs = list(
      index_freq = list(
        terms = list(
          field = "_index",
          size = 1000
        )
      )
    )
  )
  
  df = tryCatch({
    request <- elastic::Search(elastic_connexion, index = 'nomenclature', q = term, body=aggs, asdf = T)
    dd <- request$aggregations$index_freq$buckets
    dd[,1] <- toupper(dd[,1]) 
    colnames(dd)[1] <- "nomenclature"
    colnames(dd)[2] <- "occurrences"
    dd <- merge(x=dd, y=snds_nomenclatures, by = "nomenclature", all.x = T)
    dd <- dd %>% select(nomenclature, titre, occurrences)
    # the merge has shuffled the rows so we need to reorder
    dd <- dd[with(dd, order(-occurrences)), ]
    return(dd)
    },
    error=function(cond){
      print(paste0("Error when performing query to ES with term '", term, "'"))
      dd <- data.frame("-"="Pas de résultats")
      return(dd)
  })
}

get_query_result <- function(term, index, elastic_connexion){
  
  if (term==''){
    term = '*'
  }
  
  df <- tryCatch({
    request <- elastic::Search(elastic_connexion, index = index, q = term, time_scroll = "1m", size="5000")
    list_data <- list()
    nb_hits <- 1
    
    req_data <- lapply(request$hits$hits, function(x) x[["_source"]])
    list_data <- c(list_data, req_data)
    
    # If size of the hits is > than "size" option in elastic::Search, we will scroll to the next data until there is none left
    scroll_id = request$`_scroll_id`
    while (nb_hits != 0) {
      req_scroll_data <- elastic::scroll(elastic_connexion, scroll_id)
      nb_hits <- length(req_scroll_data$hits$hits)
      if (nb_hits > 0) {
        scroll_data <- lapply(req_scroll_data$hits$hits, function(x) x[["_source"]])
        list_data <- c(list_data, scroll_data)
      }
    }
    
    all_data <- bind_rows(list_data)
    return(all_data)
  },
  error=function(cond){
    print(paste0("Error when performing query to ES with term '", term, "' in the ", index, " nomenclature"))
    dd <- data.frame("-"="Pas de résultats")
    return(dd)
  })
}
